package az.ingress.designpattern.creational.abstractfactory;

public interface Notification {
    void notifyUser();
}