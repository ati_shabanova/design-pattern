package az.ingress.designpattern.creational.singleton;

public enum EnumSingleton {

    INSTANCE;

    public static void doSomething() {

    }
}
