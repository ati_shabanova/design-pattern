package az.ingress.designpattern.creational.builder;

public class Test {


    public static void main(String[] args) {
        BankAccount account = new BankAccount.Builder(1234L)
                .withOwner("Aynur")
                .atBranch("UBM")
                .openingBalance(100)
                .atRate(3.5)
                .build();

        BankAccount anotherAccount = new BankAccount.Builder(4567L)
                .withOwner("Bahadir")
                .atBranch("SUM")
                .openingBalance(400)
                .atRate(2.5)
                .build();
        System.out.println(account);
        System.out.println(anotherAccount);
    }

}
