package az.ingress.designpattern.creational.builder;

public class BankAccount {
    long accountNumber;
    String owner;
    String branch;
    double balance;
    double interestRate;

    public BankAccount() {

    }

    public static class Builder extends BankAccount {

        public Builder(long accountNumber) {
            super();
            this.accountNumber = accountNumber;
        }

        public Builder withOwner(String owner) {
            this.owner = owner;

            return this;
        }

        public Builder atBranch(String branch) {
            this.branch = branch;

            return this;
        }

        public Builder openingBalance(double balance) {
            this.balance = balance;

            return this;
        }

        public Builder atRate(double interestRate) {
            this.interestRate = interestRate;

            return this;
        }

        public BankAccount build() {

            BankAccount account = new BankAccount();
            account.accountNumber = this.accountNumber;
            account.owner = this.owner;
            account.branch = this.branch;
            account.balance = this.balance;
            account.interestRate = this.interestRate;

            return account;
        }
    }

}
